package net.mipper.tables.gdx;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {

	public static void main (String[] arg) {
		LwjglApplicationConfiguration gdxConfig = new LwjglApplicationConfiguration();
//		config.initialBackgroundColor = Color.WHITE;
        final LwjglApplication lwjglApplication = new LwjglApplication(new TimesTableGame(), gdxConfig);
	}

}
