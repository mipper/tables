package net.mipper.tables.gdx;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import net.mipper.tables.engine.QuizConfig;

// TODO
// * Configuration: skin?
// * https://github.com/libgdx/libgdx/wiki/Continuous-and-Non-Continuous-Rendering
// * Record scores and progress
//   * Graphs?
//   * Improvement
//   * Broken down by table(s)?
public class TimesTableGame extends Game {

    private Skin _skin;
    private QuizConfig _config;
    private MenuScreen _screen;

    @Override
    public void create() {
        _config = new GdxQuizConfig();
        _skin = loadSkin("comic");
//        setSkin("clean-crispy");
//        setSkin("cloud-form");
        _screen = new MenuScreen(this);
        setScreen(_screen);
    }

    @Override
    public void dispose() {
        _skin.dispose();
        super.dispose();
    }

    public QuizConfig getConfig() {
        return _config;
    }

    public Skin getSkin() {
        return _skin;
    }

    public void menu() {
        setScreen(_screen);
    }

    public void settings() {
        setScreen(new ConfigurationScreen(this));
    }

    public void play() {
        setScreen(new GameScreen(this));
    }

    private static Skin loadSkin(final String name) {
        return new Skin(Gdx.files.internal(String.format("%s/%s-ui.json", name, name)));
    }

}
