package net.mipper.tables.gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter.DigitsOnlyFilter;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pools;
import net.mipper.tables.engine.Question;
import net.mipper.tables.engine.Quiz;

public class GameScreen extends ScreenAdapter {

    private static final char BACKSPACE = 8;
    private static final int ENTER = 66;

    private Quiz _quiz;
    private Question _question;
    private TimesTableGame _game;
    private Stage _stage;
    private Label _questionLabel;
    private TextField _answerField;
    private Label _errorLabel;
    private Label _score;
    private Label _timer;
    private Stopwatch _stopwatch;
    private Sound _correct;
    private Sound _incorrect;

    public GameScreen(final TimesTableGame game) {
        _game = game;
    }

    @Override
    public void show() {
        setupSound();
        setupWindow();
        startQuiz();
    }

    public void resize (int width, int height) {
        _stage.getViewport().update(width, height, true);
    }

    @Override
    public void render(final float delta) {
        Gdx.gl.glClearColor(.7f, .7f, .95f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        displayTimeRemaining();

        _stage.draw();
        _stage.act(delta);
    }

    public void dispose() {
        _stage.dispose();
        _correct.dispose();
        _incorrect.dispose();
    }

    private void setupWindow() {
        _stage = new Stage();
//        _stage.setDebugAll(true);
        final Table table = new Table(getSkin());
        table.setFillParent(true);
        _stage.addActor(table);

        table.row().top();
        buildStatus(table);

        table.row();
        buildHeader(table);

        table.row().expand();
        table.add(buildKeypad()).colspan(2);//.fill();

        table.row();
        buildFooter(table);

        // set the stage as the input processor so it will respond to clicks etc
        Gdx.input.setInputProcessor(_stage);
        _stage.setKeyboardFocus(_answerField);
    }

    private void setupSound() {
        _correct = Gdx.audio.newSound(Gdx.files.internal("sounds/correct2.ogg"));
        _incorrect = Gdx.audio.newSound(Gdx.files.internal("sounds/incorrect1.ogg"));
    }

    private void buildStatus(final Table table) {
        _score = new Label("Score: 0/0", getSkin());
        _score.setFontScale(1.7f);
        table.add(_score).left().fillX().padLeft(4f);
        _timer = new Label("Time: 0", getSkin());
        _timer.setAlignment(Align.right);
        _timer.setFontScale(1.7f);
        table.add(_timer).right().fillX().padRight(4f);
        table.row().top();
    }

    private void buildHeader(final Table table) {
//        _questionLabel.setColor(Color.WHITE);
        table.add(buildQuestionLabel()).right().uniform().padTop(30f).padRight(4f);
        table.add(buildAnswerField()).left().uniform().padTop(20f).row();
        table.add(buildErrorLabel()).colspan(2);
    }

    private void buildFooter(final Table footer) {
        footer.add(buildBackButton()).colspan(2).padBottom(24f);
    }

    private Table buildKeypad() {
        Table table = new Table();
        table.row().expand();
        TextButton button;
        for (int i = 0; i < 3; i++) {
            for (int j = 1; j <= 3; j++) {
                char value = String.valueOf(i * 3 + j).charAt(0);
                addKeypadButton(table, buildButton(value));
            }
            table.row().expand();
        }
        addKeypadButton(table, buildButton("<", BACKSPACE));
        addKeypadButton(table, buildButton('0'));

        button = new TextButton("Go", getSkin());
        table.add(button).pad(3.0f);
        button.addListener(new ChangeListener() {
            @Override
            public void changed(final ChangeEvent event, final Actor actor) {
                if (_stopwatch != null) {
                    submit();
                }
            }
        });
        return table;
    }

    private Cell<Button> addKeypadButton(final Table table, final Button button) {
        return table.add(button).pad(3.0f);
    }

    private Button buildBackButton() {
        TextButton res = new TextButton("Back", getSkin());
        res.addListener(new ChangeListener() {
            @Override
            public void changed(final ChangeEvent event, final Actor actor) {
                _game.menu();
            }
        });
        return res;
    }

    private Button buildButton(final String value, final char send) {
        TextButton button;
        button = new TextButton(String.valueOf(value), getSkin());
        button.addListener(new KeypadChangeListener(_answerField, send));
        return button;
    }

    private Button buildButton(final char value) {
        return buildButton(String.valueOf(value), value);
    }

    private Label buildQuestionLabel() {
        _questionLabel = new Label("Time Tables", getSkin());
        _questionLabel.setFontScale(2.5f);
        return _questionLabel;
    }

    private TextField buildAnswerField() {
        _answerField = new TextField("", getSkin());
        _answerField.setDisabled(true);
        _answerField.setTextFieldFilter(new DigitsOnlyFilter());
        _answerField.addListener(new InputListener() {
            @Override
            public boolean keyDown(final InputEvent event, final int keycode) {
                if (keycode == ENTER && _stopwatch != null) {
                    submit();
                    return true;
                }
                return super.keyDown(event, keycode);
            }
        });
        return _answerField;
    }

    private Label buildErrorLabel() {
        _errorLabel = new Label("Good Luck!", getSkin());
        _errorLabel.setColor(Color.RED);
        _errorLabel.setFontScale(1.5f);
        return _errorLabel;
    }

    private void startQuiz() {
        _quiz = new Quiz(_game.getConfig().getTables());
        displayQuestion();
        _answerField.setDisabled(false);
        _stopwatch = new Stopwatch(_game.getConfig().getSeconds());
    }

    private void displayTimeRemaining() {
        if (_stopwatch != null) {
            final int timeRemaining = _stopwatch.timeRemaining();
            _timer.setText(String.format("Time: %d", timeRemaining));
            if (timeRemaining <= 0) {
                finish();
            }
        }
    }

    private void submit() {
        try {
            int answer = Integer.parseInt(_answerField.getText());
            if (_quiz.submit(_question, answer)) {
                _correct.play();
                displayQuestion();
            }
            else {
                oops();
                displayError("Not quite right.");
            }
            updateScore();
        }
        catch (NumberFormatException e) {
            oops();
            displayError(String.format("That wasn't a number! %s", _answerField.getText()));
        }
        _answerField.setText("");
    }

    private void oops() {
        _incorrect.play();
    }

    private void updateScore() {
        String progress = String.format("Score: %d/%d", _quiz.correctCount(), _quiz.questionCount());
        _score.setText(progress);
    }

    private void finish() {
        _stopwatch = null;
        _questionLabel.setText("Time Tables");
        _answerField.setDisabled(true);
        _answerField.setText("");
        int prev = updateBestScore();
        showResults(prev);
    }

    private void showResults(final int previousHigh) {
        Dialog d = new Dialog("", getSkin());
        d.getBackground().setMinWidth(300);
        d.getBackground().setMinHeight(200);
        TextButton ok = new TextButton("OK", _game.getSkin());
        ok.addListener(new ChangeListener () {
            @Override
            public void changed(final ChangeEvent event, final Actor actor) {
                _game.menu();
            }
        });
        d.button(ok);
        String results = formatResults(previousHigh);
        Label label = new Label(results, getSkin());
        label.setFontScale(2f);
        d.text(label);
        d.show(_stage);
    }

    private int updateBestScore() {
        Preferences prefs = Gdx.app.getPreferences("TimesTables/scores");
        final int currentHigh = prefs.getInteger("high", 0);
        if (currentHigh < _quiz.correctCount()) {
            prefs.putInteger("high", _quiz.correctCount());
            prefs.flush();
        }
        return currentHigh;
    }

    private void displayError(final String format) {
        _errorLabel.setText(format);
    }

    private void displayQuestion() {
        _question = generateQuestion();
        _questionLabel.setText(format(_question));
        _answerField.setText("");
        _errorLabel.setText("");
    }

    private Question generateQuestion() {
        Question question;
        do {
            question = _quiz.nextQuestion();
        } while (question.equals(_question));
        return question;
    }

    private static String format(final Question question) {
        return String.format("%d x %d =", question.getColumn(), question.getRow());
    }

    private static final class KeypadChangeListener extends ChangeListener {

        private final TextField _textField;
        private final char _value;

        private KeypadChangeListener(final TextField textField, final char value) {
            _textField = textField;
            _value = value;
        }

        @Override
        public void changed(ChangeEvent event, Actor actor) {
            InputEvent inputEvent = Pools.obtain(InputEvent.class);
            inputEvent.setCharacter(_value);
            inputEvent.setType(InputEvent.Type.keyTyped);
            try {
                _textField.fire(inputEvent);
            } finally {
                Pools.free(inputEvent);
            }
        }

    }

    private String formatResults(final int previousHigh) {
        String exclamation = "";
        if (previousHigh < _quiz.correctCount()) {
            exclamation = String.format("%n%nNew High Score!%nPrevious was %d", previousHigh);
        }
        return String.format("%d out of %d!%s",
                             _quiz.correctCount(),
                             _quiz.questionCount(),
                             exclamation);
    }

    private Skin getSkin() {
        return _game.getSkin();
    }

}