package net.mipper.tables.gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class MenuScreen extends ScreenAdapter {

    private TimesTableGame _game;
    private Stage _stage;

    public MenuScreen(final TimesTableGame game) {
        _game = game;
    }

    @Override
    public void show() {
        _stage = new Stage(new ScreenViewport());
        final Table container = new Table();
        container.setFillParent(true);
        container.add(createPlay()).padBottom(10f).row();
        container.add(createSettings()).row();
        container.add(createExit()).padTop(80f).row();
        _stage.addActor(container);
        Gdx.input.setInputProcessor(_stage);
        super.show();
    }

    @Override
    public void render(final float delta) {
        Gdx.gl.glClearColor(.7f, .7f, .95f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        _stage.draw();
        _stage.act(delta);
    }

    @Override
    public void hide(){
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        _stage.dispose();
        super.dispose();
    }

    private Button createPlay() {
        final TextButton button = new TextButton("Play", _game.getSkin());
        button.addListener(new ChangeListener() {
            @Override
            public void changed(final ChangeListener.ChangeEvent event, final Actor actor) {
                _game.play();
            }
        });
        return button;
    }

    private Actor createSettings() {
        final TextButton button = new TextButton("Settings", _game.getSkin());
        button.addListener(new ChangeListener() {
            @Override
            public void changed(final ChangeEvent event, final Actor actor) {
                _game.settings();
            }
        });
        return button;
    }

    private Actor createExit() {
        final TextButton button = new TextButton("Exit", _game.getSkin());
        button.addListener(new ChangeListener() {
            @Override
            public void changed(final ChangeEvent event, final Actor actor) {
                Gdx.app.exit();
            }
        });
        return button;
    }

}
