package net.mipper.tables.gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import net.mipper.tables.engine.QuizConfig;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GdxQuizConfig implements QuizConfig {

    private final Preferences _preferences;
    private final Set<Integer> _tables;

    public GdxQuizConfig() {
        _preferences = Gdx.app.getPreferences("TimesTables/settings");
        _tables = loadTables();
    }

    public Set<Integer> getTables() {
        return _tables;
    }

    public int getSeconds() {
        return _preferences.getInteger("seconds", 60);
    }

    public void setSeconds(final int seconds) {
        _preferences.putInteger("seconds", seconds);
    }

    public void save() {
        _preferences.putInteger("tableCount", getTables().size());
        Iterator<Integer> tableItr = getTables().iterator();
        for (int i = 0; i < getTables().size(); i++) {
            _preferences.putInteger("table" + i, tableItr.next());
        }
        _preferences.flush();
    }

    private Set<Integer> loadTables() {
        int tableCount = _preferences.getInteger("tableCount", 0);
        Set<Integer> tables = new HashSet<>(tableCount);
        for (int i = 0; i < tableCount; i++) {
            tables.add(_preferences.getInteger("table" + i));
        }
        return tables;
    }

}
