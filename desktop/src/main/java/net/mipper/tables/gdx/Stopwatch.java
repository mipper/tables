package net.mipper.tables.gdx;

import com.badlogic.gdx.utils.TimeUtils;

public class Stopwatch {

    private final int _timeAllowed;
    private final long _start;

    public Stopwatch(final int timeAllowed) {
        _timeAllowed = timeAllowed;
        _start = TimeUtils.millis();
    }

    public int timeRemaining() {
        return (int) (_timeAllowed - TimeUtils.timeSinceMillis(_start)/1000);
    }

}
