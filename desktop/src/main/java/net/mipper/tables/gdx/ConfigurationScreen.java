package net.mipper.tables.gdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class ConfigurationScreen extends ScreenAdapter {

    private final TimesTableGame _game;
    private Stage _stage;
    private TextField _timeField;

    public ConfigurationScreen(final TimesTableGame game) {
        _game = game;
    }

    @Override
    public void show() {
        _stage = new Stage(new ScreenViewport());
        Table container = new Table();
        container.setFillParent(true);
        container.add(createLabel()).right();
        container.add(createTimeField()).left();
        for(int i = 1; i <= 12; i++) {
            TextButton button = new TextButton(String.valueOf(i), _game.getSkin());
            button.setChecked(_game.getConfig().getTables().contains(i));
            button.getStyle().checked = _game.getSkin().newDrawable("button-pressed", Color.BLUE);
            button.addListener(new ChangeListener() {
                @Override
                public void changed(final ChangeEvent event, final Actor actor) {
                    final int table = Integer.parseInt(button.getText().toString());
                    if (button.isChecked()) {
                        _game.getConfig().getTables().add(table);
                    }
                    else {
                        _game.getConfig().getTables().remove(table);
                    }
                }
            });
            if (i % 2 == 1) {
                container.row();
                container.add(button).right();
            }
            else {
                container.add(button).left();
            }
        }
        container.row().bottom().expandX();
        final TextButton ok = new TextButton("Done", _game.getSkin());
        ok.addListener(new ChangeListener() {
            @Override
            public void changed(final ChangeEvent event, final Actor actor) {
                _game.getConfig().setSeconds(Integer.parseInt(_timeField.getText()));
                _game.getConfig().save();
                _game.menu();
            }
        });
        container.add(ok).colspan(2).right();
        _stage.addActor(container);
        Gdx.input.setInputProcessor(_stage);


        super.show();
    }

    @Override
    public void render(final float delta) {
        Gdx.gl.glClearColor(.7f, .7f, .95f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        _stage.draw();

        _stage.act(delta);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
        super.hide();
    }

    private Label createLabel() {
        final Label res = new Label("Time: ", _game.getSkin());
        res.setFontScale(2f);
        return res;
    }

    private TextField createTimeField() {
        _timeField = new TextField(String.valueOf(_game.getConfig().getSeconds()), _game.getSkin());
        _timeField.setTextFieldFilter(new TextField.TextFieldFilter.DigitsOnlyFilter());
        return _timeField;
    }

}
