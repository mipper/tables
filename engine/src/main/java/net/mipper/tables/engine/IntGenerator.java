package net.mipper.tables.engine;

import java.util.Random;

public class IntGenerator {

    private final Random _generator;

    public IntGenerator(final long seed) {
        _generator = new Random(seed);
    }

    public IntGenerator() {
        _generator = new Random();
    }

    public int generate(int min, int max) {
        return _generator.nextInt((max - min) + 1) + min;
    }
}