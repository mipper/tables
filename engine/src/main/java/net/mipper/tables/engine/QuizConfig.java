package net.mipper.tables.engine;

import java.util.Set;

public interface QuizConfig {

    Set<Integer> getTables();
    int getSeconds();
    void setSeconds(final int seconds);
    void save();

}
