package net.mipper.tables.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Quiz {

    private final List<Table> _tables;
    private final IntGenerator _generator;
    private int _questionCount;
    private int _correctCount;

    public Quiz(final Set<Integer> tables) {
        _generator = new IntGenerator();
        _tables = generateTables(tables);
    }

    public Question nextQuestion() {
        return _tables.get(_generator.generate(0, _tables.size() - 1)).generateQuestion();
    }

    public int questionCount() {
        return _questionCount;
    }

    public int correctCount() {
        return _correctCount;
    }

    public boolean submit(final Question question, final int answer) {
        _questionCount++;
        final boolean correct = question.mark(answer);
        if (correct) {
            _correctCount++;
        }
        return correct;
    }

    private List<Table> generateTables(final Set<Integer> tables) {
        List<Table> res = new ArrayList<>(tables.size());
        for (Integer i : tables) {
            res.add(new Table(i, _generator));
        }
        return res;
    }

}
