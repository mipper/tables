package net.mipper.tables.engine;

public class Question {

    private final int _row;
    private final int _column;

    public Question(final int column, final int row) {
        _row = row;
        _column = column;
    }

    public int answer() {
        return _row * _column;
    }

    public boolean mark(final int answer) {
        return answer == answer();
    }

    public int getRow() {
        return _row;
    }

    public int getColumn() {
        return _column;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Question) {
            Question question = (Question) obj;
            return _row == question._row && _column == question._column;
        }
        return false;
    }

}
