package net.mipper.tables.engine;

public class Table {

    private final int _number;
    private final IntGenerator _generator;

    public Table(final int number, final IntGenerator generator) {
        _number = number;
        _generator = generator;
    }

    public Question generateQuestion() {
        return new Question(_generator.generate(1, 12), _number);
    }

}
