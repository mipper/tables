package net.mipper.tables;

import net.mipper.tables.engine.Question;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class QuestionTest {

    @DataProvider
    public static Object[][] products() {
        return new Integer[][]{
                {3, 7, 20, 21},
                {7, 6, 43, 42},
        };
    }

    @Test(dataProvider = "products")
    public void testGenerate(final int row, final int column, final int wrong, final int product) {
        final Question question = new Question(column, row);
        assertThat(question.getRow()).isEqualTo(row);
        assertThat(question.getColumn()).isEqualTo(column);
        assertThat(question.answer()).isEqualTo(product);
        assertThat(question.mark(wrong)).isFalse();
        assertThat(question.mark(product)).isTrue();
    }

}