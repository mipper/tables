package net.mipper.tables;

import net.mipper.tables.engine.IntGenerator;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@Test
public class IntGeneratorTest {

    // Guarantee the sequence of generated numbers
    private static final long SEED = 1010101;

    public void testGenerator() {
        IntGenerator out = new IntGenerator(SEED);
        assertThat(out.generate(1, 12)).isEqualTo(5);
        assertThat(out.generate(1, 12)).isEqualTo(10);
        assertThat(out.generate(1, 12)).isEqualTo(7);
    }

    public void testGeneratorDifferentSeed() {
        IntGenerator out = new IntGenerator(SEED + 1);
        assertThat(out.generate(1, 12)).isEqualTo(12);
        assertThat(out.generate(1, 12)).isEqualTo(6);
        assertThat(out.generate(1, 12)).isEqualTo(11);
    }

    public void testGeneratesFullRange() {
        IntGenerator out = new IntGenerator();
        Set<Integer> allValues = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                                       .collect(Collectors.toCollection(HashSet::new));
	    Set<Integer> generated = new HashSet<>();
        while(!allValues.isEmpty()) {
            Integer v = out.generate(1, 10);
            allValues.remove(v);
            generated.add(v);
        }
        assertThat(generated.size()).isEqualTo(10);
    }

}