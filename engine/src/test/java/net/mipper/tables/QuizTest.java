package net.mipper.tables;

import net.mipper.tables.engine.IntGenerator;
import net.mipper.tables.engine.Question;
import net.mipper.tables.engine.Quiz;
import org.testng.annotations.Test;
import org.testng.collections.Sets;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@Test
public class QuizTest {

    public void testGenerateQuestion_oneTable() {
        Quiz quiz = new Quiz(Sets.newHashSet(7));
        assertThat(quiz.questionCount()).isEqualTo(0);
        assertThat(quiz.questionCount()).isEqualTo(0);

        Question question = quiz.nextQuestion();
        assertThat(question.getRow()).isEqualTo(7);
        int col = question.getColumn();
        quiz.submit(question, 7 * col);
        assertThat(quiz.questionCount()).isEqualTo(1);
        assertThat(quiz.correctCount()).isEqualTo(1);

        question = quiz.nextQuestion();
        assertThat(question.getRow()).isEqualTo(7);
        quiz.submit(question, -1);
        assertThat(quiz.questionCount()).isEqualTo(2);
        assertThat(quiz.correctCount()).isEqualTo(1);
    }

    public void testGenerateQuestion_multipleTables() {
        Set<Integer> tables = Stream.of(6, 7, 8, 9).collect(Collectors.toCollection(HashSet::new));
        Set<Integer> columns = new HashSet<>();
        Quiz out = new Quiz(tables);
        int count = 0;
        while(!tables.isEmpty() && columns.size() < 4) {
            count++;
            final Question question = out.nextQuestion();
            System.out.printf("%d * %d = %d%n", question.getRow(), question.getColumn(), question.answer());
            tables.remove(question.getRow());
            columns.add(question.getColumn());
            out.submit(question, question.answer());
            assertThat(out.questionCount()).isEqualTo(count);
            assertThat(out.correctCount()).isEqualTo(count);
        }
    }

}