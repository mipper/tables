package net.mipper.tables;

import net.mipper.tables.engine.IntGenerator;
import net.mipper.tables.engine.Question;
import net.mipper.tables.engine.Table;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class TableTest {

    private static final Integer[] GENERATED = {7, 5, 6, 1, 9, 12};

    @Mock
    private IntGenerator _intGenerator;

    @BeforeClass
    protected void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testGenerateQuestion() {
        for(int i = 1; i <= 12; i++) {
            when(_intGenerator.generate(1, 12)).thenReturn(10, GENERATED);
            Table out = new Table(i, _intGenerator);
            assertQuestion(out, i, 10);
            for(int j = 0; j < GENERATED.length; j++){
                assertQuestion(out, i, GENERATED[j]);
            }
        }
    }

    private void assertQuestion(final Table out, final int i, final int j) {
        final Question question = out.generateQuestion();
        assertThat(question.getRow()).isEqualTo(i);
        assertThat(question.getColumn()).isEqualTo(j);
        assertThat(question.answer()).isEqualTo(i * j);
    }

}